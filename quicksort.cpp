#include <iostream>
#include <vector>

using namespace std;

template <typename Type>
vector<Type> concatenate(vector<Type> less, Type pivot, vector<Type> greater);

template <typename Type>
vector<Type> quickSort(vector<Type> array);

int main()
{
    vector<int> a {10,5,1,12,4,3,4,9,8,2};
    vector<int> b;

    b = quickSort(a);

    for(auto v:b)
        cout << v << endl;

    return 0;
}

template <typename Type>
vector<Type> concatenate(vector<Type> less, Type pivot, vector<Type> greater)
{
    vector<Type> concatenated;
    concatenated = less;
    concatenated.push_back(pivot);
    for(auto v : greater)
        concatenated.push_back(v);
    return concatenated;
}

template <typename Type>
vector<Type> quickSort(vector<Type> array)
{
    if(array.size() <= 1)
        return array;

    Type pivot = array[array.size() / 2];
    array.erase(array.begin() + array.size() / 2);

    vector<Type> less, greater;
    for(auto v : array)
    {
        if(v <= pivot)
            less.push_back(v);
        else
            greater.push_back(v);
    }

    return concatenate(quickSort(less), pivot, quickSort(greater));
}















